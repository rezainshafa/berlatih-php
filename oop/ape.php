<?php

require_once("animal.php");

class Ape extends Animal {
    public $leg = 2;
    public $yell = "Auooo";

    public function yell() {

        echo "Yell : " . $this->yell;

    }

}

?>